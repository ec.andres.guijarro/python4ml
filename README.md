# Python4ML

### The Complete Python Course

#### Chapter 1: Getting Started

##### Important Dates
* Start Date: 27/01/2020
* End Date  : 27/01/2020

##### Exercises:
* Exercises related to environment config.

#### Chapter 2: Primitive Types

##### Important Dates
* Start Date: 28/01/2020
* End Date  : 30/01/2020

##### Exercises:
* Variables.
* Strings & String Methods
* Formatted Strings
* Escaped Sequences
* Numbers
* Type Conversion

#### Chapter 3: Control Flow

##### Important Dates
* Start Date: 30/01/2020
* End Date  : 30/01/2020

##### Exercises:
* ChainingComparisonOperators
* Comparison Operators
* For else
* For loop
* Iterables
* Logical Operators
* Nested Loops
* Ternary Operator
* While Loop

#### Chapter 4: Functions

##### Important Dates
* Start Date: 31/01/2020
* End Date  : 31/01/2020

##### Exercises:

* Arguments
* Default Argument
* Defining Function
* Exercise
* Keyword Argument
* Scope
* Type Function
* xargs
* xxargs

#### Chapter 5: Data Structures

##### Important Dates
* Start Date: 31/01/2020
* End Date  : 31/01/2020

##### Exercises:

* Lists
* Arrays
* Tuples
* Sets
* Dictionaries
* Generator Objects

#### Chapter 9: Python Standard Library

##### Important Dates
* Start Date: 31/01/2020
* End Date  : 31/01/2020

##### Exercises:

* Lists
* Arrays
* Tuples
* Sets
* Dictionaries
* Generator Objects