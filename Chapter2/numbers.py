# Types of numbers
x = 1
y = 1.1
z = 5 + 7j

print("Integer:", x)
print("Float:", y)
print("Complex:", z)

#Standar Operators

print('Add:', 5 + 8)
print('Subtraction:', 8 - 5)
print('Multiplication:', 5 * 8)
print('Division:', 5 / 8)
print('Division:', 5 // 8)
print('Module:', 5 % 8)
print('Exponent:', 5 ** 2)


#Augmented Operators

x = 10
print(x)

x = x + 3
print (x)

x += 3
print(x)