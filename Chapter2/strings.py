# Strings
course_name = "Python Programing"
# Number of characters
print(len(course_name))
# Get the first character
print(course_name[0])
# Get the last character
print(course_name[-1])
# Get substrings
print(course_name[0:3])
print(course_name[:3])
print(course_name[0:])
print(course_name[:])
