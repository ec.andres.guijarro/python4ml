course = "   Python    Programming   "

# String Len
print(len(course))

# String Methods
print("Upper: ", course.upper())
print("Lower: ", course.lower())
print("Title: ", course.title())
print("Strip: ", course.strip())
print("L-Strip: ", course.lstrip())
print("R_Strip: ", course.rstrip())
print("Find: ", course.find("Pro"))
print("Find: ", course.find("pro"))
print("Replace: "), course.replace("j", "p")
print("Search", "Pro" in course)
print("Search", "Swift" in course)