x = input("x: ")
print(type(x))

# int(x)
# float(x)
# bool(x)
# str(x)

y = int(x) + 1

print(f"x:{x}, y: {y}")

# Falsy Values
# ""
# 0
# None

print("Boolean:", bool(""))
print("Boolean:", bool(0))
print("Boolean:", bool(None))