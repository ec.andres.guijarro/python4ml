# Option 1
#age = 22
#if age >= 18:
#	message = "Eligible"
#else:
#	message = "No Eligible"
#print(message)

# Option 2 -> ternaryOperator
age = 22
message = "Eligible" if age >=18 else "No Eligible"
print(message)