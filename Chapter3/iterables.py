print(type(5))
print(type(range(5)))

for number in range(5):
	print(f"Number: {number}")


for x in "Python":
	print(f"x: {x}")


for y in [10, 20, 34, "Python"]:
	print(f"y: {y}")