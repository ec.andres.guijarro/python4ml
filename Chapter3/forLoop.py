
# Option 1
print("Option 1")
for number in range(3):
	print("Attempt:", number + 1, (number + 1) * ".")

# Option 2
print("Option 2")
for number in range(1, 4):
	print("Attempt:", number, (number) * ".")

# Option 3
print("Option 3")
for number in range(1, 10, 2):
	print("Attempt:", number, (number) * ".")	