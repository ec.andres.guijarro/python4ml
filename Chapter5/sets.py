# set is not duplicates
# set does not have index

numbers = [1, 2, 1, 2, 4, 5]
uniques = set(numbers)

print(uniques)
second = {1, 5}
second.add(5)
print(second)
second.remove(5)
print(second)

# Operations

first = {1, 1, 2, 3, 4}
second = {1, 5}

print(first | second)

print(first & second)

print(first - second)

print(first ^ second)

if 1 in first:
	print("Exist")
