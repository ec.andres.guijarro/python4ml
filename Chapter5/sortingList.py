#Option 1
numbers = [3, 51, 2, 8, 6]
numbers.sort(reverse=True)
print(numbers)

#Option 2
print(sorted(numbers, reverse=True))


def sort_items(items):
	return items[1]

items = [("product1", 10),("product2", 9), ("product3", 12)]
items.sort(key=sort_items)
print(items)

