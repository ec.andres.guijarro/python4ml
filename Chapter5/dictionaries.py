point = {"x": 1, "y":5}
print(point)

point_location = dict(x=1, y=2)
print(point_location)


user_info = dict(name = "John", age = 36, country = "Ecuador")
print(user_info)

print(user_info["name"])

# Update data
user_info["name"] = "Andres"
print(user_info["name"])

# Add data
user_info["city"] = "Quito"
print(user_info)


# Verify if value exists
if "a" in point:
	print(point["a"])

print(point.get("a", 0))


#Delete data
del point["x"]
print(point)

# Looping data -
for key in user_info:
	print(key, user_info[key])

for key, value in user_info.items():
	print(key, value)

