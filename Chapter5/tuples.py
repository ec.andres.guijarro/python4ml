point = (1, 2, 3)
point_1 = 1,
point_2 = 1, 2
point_3 = () 
point_4 = (4, 5, 6)

print(type(point))
print(type(point_1))
print(type(point_2))
print(type(point_3))

print(point + point_4)

print(point * 4)


point_5 = tuple("Hello World")
print(point_5)
print(point_5[0:2])

x, y, z = point
print(f"x: {x}")
print(f"y: {y}")
print(f"y: {z}")

if 10 in point:
	print("Existe")
else:
	print("No existe")

#Tuples no add or remove elements