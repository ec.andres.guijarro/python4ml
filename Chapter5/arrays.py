from array import array

numbers = array("i",  [1, 2, 3])

print(numbers)
print(type(numbers))

numbers.append(4)
numbers.insert(2, 8)

print(numbers)

numbers.pop()
numbers.pop(1)

print(numbers)

numbers.remove(2)

print(numbers)

