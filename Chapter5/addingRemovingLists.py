numbers = [1, 2, 3, 4]


#Add value at the end

numbers.append(5)
print(numbers)

#Add value at any position

numbers.insert(0, "-")
print(numbers)

numbers.insert(3, "-")
print(numbers)


#Remove value

numbers.pop()
print(numbers)

numbers.pop(4)
print(numbers)


numbers.remove("-")
print(numbers)


del numbers[0:3]
print(numbers)

numbers.clear()
print(numbers)


