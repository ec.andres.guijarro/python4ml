items = [("product1", 10),("product2", 9), ("product3", 12)]


# Instead of map function

prices = [item[1] for item in items]
print(prices)

# Instead of filter function
filter_prices = [item for item in items if item[1] >=10]
print(filter_prices)