from pprint import pprint

sentence = "This is a common interview question"

occurrences = {}

for char in sentence:
	if char in occurrences:
		occurrences[char] += 1
	else:
		occurrences[char] = 1


occurrences_sorted = sorted(occurrences.items(), 
							key = lambda kv : kv[1], 
							reverse=True)
print(occurrences_sorted[0])