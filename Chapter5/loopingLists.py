numbers = [1, 2, 3, 4]

for number in numbers:
	print(number)

for number in enumerate(numbers):
	print(f"Index: {number[0]} , value: {number[1]}")

for index, number in enumerate(numbers):
	print(f"Index: {index} , value: {number}")






