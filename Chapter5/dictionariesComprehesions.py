#set
values = {x * 2 for x in range(5)}
print(values)


#dictionary
values_dictionary = {x: x * 2 for x in range(5)}
print(values_dictionary)
