numbers = [1, 2, 3, 4]

first, second, *other_number = numbers

print(first)
print(second)
print(other_number)

first, *other_number, last  = numbers
print(first)
print(other_number)
print(last)






