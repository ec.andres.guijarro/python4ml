#Lists

numbers = [1, 2, 3]

print(numbers)

print(*numbers)

values = [*range(5), *"Hello"]
print(values)


first = [1, 2, 3]
second = [4, 5]

values = [*first, "a", *second]
print(values)


#Dictionaries

first = dict(x = 1)
second = dict(x = 10, y = 2)

combined = {**first, **second}
print(combined)