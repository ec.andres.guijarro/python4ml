# 1.- Perform task
def greet(first_name, last_name):
	print(f"Hi { first_name} {last_name} ")
	print("Welcome aboard")

greet("Andres", "Guijarro")
greet("Victor", "Guijarro")

#2.- Return value
def get_gretting(name):
	return f"Hi {name}"

message = get_gretting("Andres Guijarro")
file = open("context.txt", "w")
file.write(message)