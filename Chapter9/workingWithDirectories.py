from pathlib import Path

path = Path("../Chapter9")
#path.exists()
#path.mkdir()
#path.rmdir()
#path.rename("Chapter2")

print(path)

for p in path.iterdir():
	print(p)


paths = [p for p in path.iterdir()]
print(paths)

paths = [p for p in path.iterdir() if p.is_dir()]
print(paths)

py_files = [p for p in path.glob("*.py")]
print(py_files)

py_files = [p for p in path.rglob("*.py")]
print(py_files)