from pathlib import Path

#print(Path(r"C:\Users\victo\SoftwareCode\ML\python4ml\Chapter9"))
#print(Path())
#print(Path() / "SoftwareCode"/"ML"/"python4ml"/"Chapter9")
#print(Path.home())

print(Path("./Chapter9/"))

path = Path("./Chapter9")
print(path.exists())
print(path.is_file())
print(path.is_dir())

print(path.name)
print(path.stem)
print(path.suffix)
print(path.parent)

path = path.with_name("file.txt")
print(path)

path = path.with_suffix(".txt")
print(path)

print(path.absolute())
