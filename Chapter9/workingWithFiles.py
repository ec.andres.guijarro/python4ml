from pathlib import Path
from time import ctime
import shutil

path = Path("../Chapter5/accesingLists.py")

if path.exists():
	print("Archivo Existe")

#rename file
#path.rename("accesingLists1.py")

#delete file
#path.unlink()

print(path.stat())
print(ctime(path.stat().st_ctime))

print(path.read_bytes())
print(path.read_text())



source = Path("../Chapter5/accesingLists.py")
target = Path("../Chapter9") / "accesingListsTest.py"

target.write_text(source.read_text())

shutil.copy(source, target)